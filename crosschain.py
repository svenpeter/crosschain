'''
	CrossChain - automatic embedded toolchain (binutils,gcc) builder
	Copyright (C) 2014 Sven Peter <sven@ha.cki.ng>
	Released under the terms of the GNU GPL, version 2

	Based on builtit.git Copyright (C) 2007-2009 Segher Boessenkool <segher@kernel.crashing.org>
	Released under the terms of the GNU GPL, version 2
	http://git.infradead.org/users/segher/buildall.git
'''

import sys, os, subprocess, shutil, datetime
from termcolor import colored

BINUTILS_VERSION = "2.24"
GCC_VERSION = "4.8.2"
GMP_VERSION = "5.1.3"
MPC_VERSION = "1.0.2"
MPFR_VERSION = "3.1.2"

triplets = {
	"arm": "arm-eabi",
	"armeb": "armeb-eabi",
	"ppc": "powerpc-elf",
	"mips": "mips-elf",
	"x86": "i386-elf",
	"x86_64": "x86_64-elf",
}

class CrossChain(object):

	def __init__(self, prefix, target, n_jobs = 1):
		self.prefix = prefix
		self.target = target
		self.n_jobs = n_jobs

		self.tmpdir = os.path.join(self.prefix, "downloads")
		self.builddir = os.path.join(self.prefix, "build")

		if target not in triplets:
			raise NotImplementedError("%s is unsupported" % target)

		self.triplet = triplets[target]

		for d in [self.tmpdir, self.builddir]:
			if os.path.exists(d) != True:
				os.mkdir(d)
			if os.path.isdir(d) != True:
				raise IOError("%s not a dir" % d)

		self.BINUTILS_VERSION = BINUTILS_VERSION
		self.GCC_VERSION = GCC_VERSION
		self.GMP_VERSION = GMP_VERSION
		self.MPC_VERSION = MPC_VERSION
		self.MPFR_VERSION = MPFR_VERSION

	def call(self, display, args, log):
		print colored(display, "green"),
		sys.stdout.flush()


		logfile = open(os.path.join(self.builddir, "%s.%s" % (self.target, log)), "w")

		start = datetime.datetime.now()
		status = subprocess.call(args, stdout = logfile, stderr = logfile)
		end = datetime.datetime.now()

		delta = end - start

		if status == 0:
			print colored("success! total time: %d seconds" % delta.seconds, "green")
		else:
			print colored("failure!", "red")
			sys.exit(1)

	def print_center(self, s):
		print colored(("  %s  " % s).center(80, "#"), "yellow")

	def download(self, target, url):
		fulltarget = os.path.join(self.tmpdir, target)

		self.call("Downloading %s..." % target, ["wget", "--continue", url, "-O", fulltarget], "binutils.download")


	def download_all(self):
		self.print_center("Downloading sources")
		files = [
					"ftp://ftp.gnu.org/gnu/binutils/binutils-%s.tar.bz2" % self.BINUTILS_VERSION,
					"ftp://ftp.gnu.org/gnu/gcc/gcc-%s/gcc-%s.tar.bz2" % (self.GCC_VERSION, self.GCC_VERSION),
					"ftp://ftp.gmplib.org/pub/gmp-%s/gmp-%s.tar.bz2" % (self.GMP_VERSION, self.GMP_VERSION),
					"http://www.multiprecision.org/mpc/download/mpc-%s.tar.gz" % self.MPC_VERSION,
					"http://www.mpfr.org/mpfr-%s/mpfr-%s.tar.bz2" % (self.MPFR_VERSION, self.MPFR_VERSION)
				]

		for f in files:
			fname = f.split('/')[-1]
			self.download(fname, f)

	def build_init(self, name):
		buildpath = os.path.join(self.builddir, name)
		if os.path.exists(buildpath):
			shutil.rmtree(buildpath)
		os.mkdir(buildpath)

		cwd = os.getcwd()
		os.chdir(buildpath)

		return cwd

	def build_binutils(self):
		self.print_center("Building binutils")
		cwd = self.build_init("binutils-%s" % self.BINUTILS_VERSION)

		args = ["tar", "xvjf", os.path.join(self.tmpdir, "binutils-%s.tar.bz2" % self.BINUTILS_VERSION)]
		self.call("Unpacking binutils...", args, "binutils.unpack")

		os.chdir("binutils-%s" % self.BINUTILS_VERSION)
		os.mkdir("builddir")
		os.chdir("builddir")

		args = [
				"../configure",
				"--prefix=%s" % self.prefix,
				"--target=%s" % self.triplet,
				"--disable-nls",
				"--disable-shared",
				"--disable-debug",
				"--disable-werror",
				"--disable-dependency-tracking",
				"--with-gcc",
				"--with-gnu-as",
				"--with-gnu-ld"
		]

		self.call("Configuring binutils...", args, "binutils.configure")
		self.call("Building binutils...", ["make", "-j%d" % self.n_jobs], "binutils.make")
		self.call("Installing binutils...", ["make", "install"],  "binutils.install")

		os.chdir(cwd)

	def build_gcc(self):
		self.print_center("Building gcc")
		cwd = self.build_init("gcc-%s" % self.GCC_VERSION)

		def unpack_args(name):
			return ["tar", "xvjf", os.path.join(self.tmpdir, name)]

		self.call("Unpacking gcc...", unpack_args("gcc-%s.tar.bz2" % self.GCC_VERSION), "gcc.unpack")

		os.chdir("gcc-%s" % self.GCC_VERSION)

		self.call("Unpacking GMP...", unpack_args("gmp-%s.tar.bz2" % self.GMP_VERSION), "gmp.unpack")
		self.call("Unpacking MPC...", unpack_args("mpc-%s.tar.gz" % self.MPC_VERSION), "mpc.unpack")
		self.call("Unpacking MPFR...", unpack_args("mpfr-%s.tar.bz2" % self.MPFR_VERSION), "mpfr.unpack")

		os.symlink("gmp-%s" % self.GMP_VERSION, "gmp")
		os.symlink("mpc-%s" % self.MPC_VERSION, "mpc")
		os.symlink("mpfr-%s" % self.MPFR_VERSION, "mpfr")

		os.mkdir("builddir")
		os.chdir("builddir")

		args = [
			"../configure",
			"--prefix=%s" % self.prefix,
			"--target=%s" % self.triplet,
			"--enable-languages=c",
			"--without-headers",
			"--enable-sjlj-exceptions",
			"--with-system-libunwind",
			"--disable-nls",
			"--disable-threads",
			"--disable-shared",
			"--disable-libmudflap",
			"--disable-libssp",
			"--disable-libgomp",
			"--disable-decimal-float",
			"--disable-libquadmath",
			"--disable-dependency-tracking",
			"--disable-multilib",
			"--disable-win32-registry"
		]

		self.call("Configuring gcc...", args, "gcc.configure")
		self.call("Building gcc...", ["make", "-j%d" % self.n_jobs], "gcc.make")
		self.call("Installing gcc...", ["make", "install"],  "gcc.install")

		os.chdir(cwd)


	def build_all(self):
		self.build_binutils()
		self.build_gcc()

	def clean_all(self):
		os.chdir(self.builddir)
		for d in ["gcc-%s" % self.GCC_VERSION, "binutils-%s" % self.BINUTILS_VERSION]:
			try: shutil.rmtree(d)
			except: pass

	def go(self):
		self.download_all()
		self.build_all()
		self.clean_all()


if __name__ == "__main__":
	if "CROSSCHAIN_PREFIX" in os.environ:
		prefix = os.environ["CROSSCHAIN_PREFIX"]
	else:
		prefix = os.path.join(os.environ["HOME"], "crosschain")

	n_jobs = 1
	targets = []
	for arg in sys.argv[1:]:
		if arg.startswith("-j"):
			n_jobs = int(arg[2:])
		else:
			targets.append(arg)

	for target in targets:
		print colored(("  Building toolchain for target '%s'  " % target).center(80, "#"), "yellow")
		cc = CrossChain(prefix, target, n_jobs)
		cc.go()